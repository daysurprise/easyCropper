# easyCropper

#### 介绍

集成cropper,jquery,layui的简单裁剪图片的组件，可以自定义上传图片的尺寸，比例等信息，及其裁剪上传后可以回显裁剪后的图片。支持一个界面上多个裁剪组件。

#### 软件架构
前提必须是在layui环境下  layui官网地址：[layui官网地址](https://www.layui.com/)；

集成了cropper，cropper官网地址：[cropper官网](https://fengyuanchen.github.io/cropperjs/)

#### 功能点

##1. 图片裁剪
##2. 保存尺寸设置
##3. 默认图片回显
##4. 大小限制
##5. 裁剪框比例设置
##6. 滚轮放大缩小图片
##7. 图片旋转


#### 安装教程

1. 为一个按钮绑定easyCropper组件

```
<div class="layui-form-item">
                    <label class="layui-form-label"><i style="color: red">*</i>产品图片:</label>
                    <input type="hidden" name="productImg" id="productImg"/>
                    <div class="layui-upload">
                        <button type="button" class="layui-btn" id="productImgButton">上传</button>
                        <div class="layui-upload-list">
                            <img class="layui-upload-img" id="productImgImg">
                        </div>
                    </div>
                </div>
```


2. layui初始化时引入easyCropper

```
 layui.config({
        base: contextPath + 'layui/easyCropper/' //layui自定义layui组件目录
    })
    layui.use(['easyCropper'], function(){
      
        var easyCropper = layui.easyCropper;
        //创建一个图片裁剪上传组件
        var productImgCropper = easyCropper.render({
            elem: '#productImgButton'
            ,defaultImg: '/admin/images/demo.jpg' // 默认图片 选填
            ,size: 2048    // 大小限制 默认1024k 选填
            ,saveW:280     //保存宽度
            ,saveH:160     //保存高度
            ,mark:7/4   //选取比例
            ,area:'900px'  //弹窗宽度
            ,url: contextPath + 'upload/img'  //图片上传接口返回和（layui 的upload 模块）返回的JOSN一样
            ,done: function(url){ //上传完毕回调
                $("#productImg").val(url);
                $("#productImgImg").attr('src',url);
            }
        });
})
```


3. 然后将组件的文件夹easyCropper放到能找到的文件夹下即可，我这边是放在layui下面

![easyCropper文件夹存放路径](https://images.gitee.com/uploads/images/2019/0822/113509_182fb570_2063271.png "easyCropper.png") 

#### 使用说明

1. 注意修改cropper文件夹为你自己的地址
2. 注意easyCropper.js中css地址
3. 注意上传到服务器成功后返回的是一个图片地址
4. 默认图片 defaultImg 和 大小限制 size默认为1024k(1M) 是 选填项

#### 效果展示
![裁剪界面begin](https://images.gitee.com/uploads/images/2019/0822/114912_e5b5f365_2063271.png "easyCropper_main2.png")

![裁剪界面](https://images.gitee.com/uploads/images/2019/0822/114549_76f82815_2063271.png "easyCropper_main.png")


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
5. 欢迎大家新增更多花里胡哨的功能,哈哈

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)